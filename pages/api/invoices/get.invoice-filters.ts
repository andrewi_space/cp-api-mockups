import { NextApiRequest, NextApiResponse } from 'next'
import obj from "./get.invoice-filters.json";

export default (req: NextApiRequest , res: NextApiResponse) => {
	res.status(200).json(obj)
}
