import { NextApiRequest, NextApiResponse } from 'next'
import obj from "./get.proposal-details.json";

export default (req: NextApiRequest , res: NextApiResponse) => {
	res.status(200).json(obj)
}
