import { NextApiRequest, NextApiResponse } from 'next'
import obj from "./get.note.json";

export default (req: NextApiRequest , res: NextApiResponse) => {
	console.log('req: ', req.method, req.query)
	const obj1 = {...obj, Id: req.query.id ? req.query.id : obj.Id}
	res.status(200).json(obj1)
}
