import { NextApiRequest, NextApiResponse } from 'next'
import obj from "./post.note.json";

export default (req: NextApiRequest , res: NextApiResponse) => {
	res.status(200).json(obj)
}
