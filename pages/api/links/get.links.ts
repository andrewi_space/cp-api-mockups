import { NextApiRequest, NextApiResponse } from 'next'
import obj from "./get.links.json";

export default (req: NextApiRequest , res: NextApiResponse) => {
	res.status(200).json(obj)
}
